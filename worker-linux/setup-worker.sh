#!/bin/bash -x

# k8s requires 2 CPUs. We override this requirement (to better use resources from the Always Free tier).
# We also ignore memory requirement, as for testing we deploy cluster to micro shapes.
# Currently we don't pass control plane's CA public key.
sudo kubeadm join \
  --ignore-preflight-errors=NumCPU,Mem \
  --node-name="worker-linux" \
  --token=$(< .kube/join-token) \
  --discovery-token-unsafe-skip-ca-verification \
  10.128.0.5:6443  # kubeapi ip:port
  
# On control-plane execute 
kubectl label node ${node_name} node-role.kubernetes.io/worker=worker
