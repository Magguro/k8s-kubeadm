#!/bin/bash -x

mkdir -p .kube

kubeadm token generate > .kube/join-token
pub_ip=$(curl ifconfig.me)

# k8s requires 2 CPUs. We override this requirement (to better use resources from the Always Free tier).
# We also ignore memory requirement, as for testing we deploy cluster to micro shapes.
sudo kubeadm init \
  --ignore-preflight-errors=NumCPU,Mem \
  --control-plane-endpoint=$(hostname -i) \
  --pod-network-cidr=10.244.0.0/16 \
  --node-name=$(hostname) \
  --token=$(< .kube/join-token) \
  --apiserver-cert-extra-sans=$pub_ip

openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | \
  openssl rsa -pubin -outform der 2>/dev/null | \
  openssl dgst -sha256 -hex | \
  sed 's/^.* //' \
  > .kube/join-hash
